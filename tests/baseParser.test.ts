/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

'use strict';
import {Workbook} from '@oliveai/ldk/dist/document/types';
import {
  visitTypesParserConfigWithCustomHeader,
  visitTypesParserConfigWithoutCustomHeader,
  cpeParserConfigWithoutCustomHeader,
} from './configs/parserConfigs';
import {VisitTypesTestParser, MedicareCpeTestParser} from './ParserTestParser';

afterAll(async () => {
  console.log('Run after All test');
});

test('Testing Custom Header', async () => {
  const workbook: Workbook = {
    worksheets: [
      {
        hidden: false,
        hiddenColumns: [],
        hiddenRows: [],
        name: 'VISIT TYPES',
        rows: [
          {
            cells: [
              {value: 'category'},
              {value: 'visitType'},
              {value: 'reasonForVisit'},
              {value: 'notes'},
              {value: 'scheduledAtNavCenter'},
            ],
          },
          {
            cells: [
              {value: 'category'},
              {value: 'visitType'},
              {value: 'reasonForVisit'},
              {value: 'notes'},
              {value: 'scheduledAtNavCenter'},
            ],
          },
          {
            cells: [
              {value: 'value1r1'},
              {value: 'value2r1'},
              {value: 'value3r1'},
              {value: 'value4r1'},
              {value: 'value5r1'},
            ],
          },
          {
            cells: [
              {value: 'value1r2'},
              {value: 'value2r2'},
              {value: 'value3r2'},
              {value: 'value4r2'},
              {value: 'value5r2'},
            ],
          },
          {
            cells: [
              {value: 'value1r3'},
              {value: 'value2r3'},
              {value: 'value3r3'},
              {value: 'value4r3'},
              {value: 'value5r3'},
            ],
          },
          {
            cells: [
              {value: 'value1r4'},
              {value: 'value2r4'},
              {value: 'value3r4'},
              {value: 'value4r4'},
              {value: 'value5r4'},
            ],
          },
        ],
      },
    ],
  };

  const vtp = new VisitTypesTestParser(
    workbook,
    'VISIT TYPES',
    visitTypesParserConfigWithCustomHeader
  );

  const data = vtp.parse();

  expect(data.header[0]).toBe('category');
  expect(data.header[3]).toBe('notes');
});

test('Use Default Header', async () => {
  const workbook: Workbook = {
    worksheets: [
      {
        hidden: false,
        hiddenColumns: [],
        hiddenRows: [],
        name: 'VISIT TYPES',
        rows: [
          {
            cells: [
              {value: 'category'},
              {value: 'visitType'},
              {value: 'reasonForVisit'},
              {value: 'notes'},
              {value: 'scheduledAtNavCenter'},
            ],
          },
          {
            cells: [
              {value: 'value1r1'},
              {value: 'value2r1'},
              {value: 'value3r1'},
              {value: 'value4r1'},
              {value: 'value5r1'},
            ],
          },
          {
            cells: [
              {value: 'value1r2'},
              {value: 'value2r2'},
              {value: 'value3r2'},
              {value: 'value4r2'},
              {value: 'value5r2'},
            ],
          },
          {
            cells: [
              {value: 'value1r3'},
              {value: 'value2r3'},
              {value: 'value3r3'},
              {value: 'value4r3'},
              {value: 'value5r3'},
            ],
          },
          {
            cells: [
              {value: 'value1r4'},
              {value: 'value2r4'},
              {value: 'value3r4'},
              {value: 'value4r4'},
              {value: 'value5r4'},
            ],
          },
        ],
      },
    ],
  };
  const vtp = new VisitTypesTestParser(
    workbook,
    'VISIT TYPES',
    visitTypesParserConfigWithoutCustomHeader
  );

  const data = vtp.parse();

  expect(data.header[0]).toBe('category');
  expect(data.header[3]).toBe('notes');
});

/**
 * This test makes sure that the original data in the sheet that wasn't mapped is available.
 */
test('Get data from original sheet', async () => {
  const workbook: Workbook = {
    worksheets: [
      {
        hidden: false,
        hiddenColumns: [],
        hiddenRows: [],
        name: 'MEDICARE CPE',
        rows: [
          {
            cells: [
              {
                value:
                  'Below is a list of Medicare plans and whether they qualify for CPE. All below plans can have AWV.',
              },
            ],
          },
          {
            cells: [{value: 'INSURANCE'}, {value: 'QUALIFY FOR CPE'}],
          },
          {
            cells: [{value: 'value1r1'}, {value: 'value2r1'}],
          },
          {
            cells: [{value: 'value1r2'}, {value: 'value2r2'}],
          },
          {
            cells: [{value: 'value1r3'}, {value: 'value2r3'}],
          },
          {
            cells: [{value: 'value1r4'}, {value: 'value2r4'}],
          },
        ],
      },
    ],
  };

  const parser = new MedicareCpeTestParser(
    workbook,
    'MEDICARE CPE',
    cpeParserConfigWithoutCustomHeader
  );

  const data = parser.parse();
  expect(data.header[0]).toBe('INSURANCE');
  expect(data.header[1]).toBe('QUALIFY FOR CPE');
  expect(data.data.listMessage).toBe(
    'Below is a list of Medicare plans and whether they qualify for CPE. All below plans can have AWV.'
  );
  expect(data.data.insuranceQualification[0].insurance).toBe('value1r1');
  expect(data.data.insuranceQualification[0].qualifyForCpe).toBe('value2r1');
});
