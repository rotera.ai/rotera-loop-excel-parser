/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

export const visitTypesParserConfigWithCustomHeader = {
  dataStartRow: 3,
  customHeader: {
    useHeader: true,
    headerRow: 2,
  },
  header: {
    useHeader: true,
    headerRow: 1,
  },
};

export const visitTypesParserConfigWithoutCustomHeader = {
  dataStartRow: 2,
  header: {
    useHeader: true,
    headerRow: 1,
  },
};

export const cpeParserConfigWithoutCustomHeader = {
  customDefinedHeader: ['INSURANCE', 'QUALIFY FOR CPE'],
  dataStartRow: 3,
  header: {
    useHeader: false,
    headerRow: 2,
  },
};
