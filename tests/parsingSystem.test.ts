/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {createParsingSystem} from '../src/parsers/ParsingSystem';
import {whisper} from '@oliveai/ldk';
import {Workbook} from '@oliveai/ldk/dist/document/types';
import {Component} from '@oliveai/ldk/dist/whisper';
import {DataStore} from '../src/types/parsingSystemTypes';
import {
  assertComponentIsBox,
  assertComponentIsButton,
} from '../src/componentAssertions';
import * as utils from '../src/utils';

let loadWorkbookSpy: jest.SpyInstance;
let whisperSpy: jest.SpyInstance;
let updateMock: jest.Mock;

let workbook: Workbook;
const dataStore: DataStore = {
  errorsMuted: false,
  launchWhisperSent: false,
};
const errorMessageLabel = 'Loading Errors';

beforeAll(async () => {
  workbook = {
    worksheets: [
      {
        hidden: false,
        hiddenColumns: [],
        hiddenRows: [],
        name: 'VISIT TYPES',
        rows: [
          {
            cells: [
              {value: 'category'},
              {value: 'visitType'},
              {value: 'reasonForVisit'},
              {value: 'notes'},
              {value: 'scheduledAtNavCenter'},
            ],
          },
          {
            cells: [
              {value: 'value1r1'},
              {value: 'value2r1'},
              {value: 'value3r1'},
              {value: 'value4r1'},
              {value: 'value5r1'},
            ],
          },
          {
            cells: [
              {value: 'value1r2'},
              {value: 'value2r2'},
              {value: 'value3r2'},
              {value: 'value4r2'},
              {value: 'value5r2'},
            ],
          },
          {
            cells: [
              {value: 'value1r3'},
              {value: 'value2r3'},
              {value: 'value3r3'},
              {value: 'value4r3'},
              {value: 'value5r3'},
            ],
          },
          {
            cells: [
              {value: 'value1r4'},
              {value: 'value2r4'},
              {value: 'value3r4'},
              {value: 'value4r4'},
              {value: 'value5r4'},
            ],
          },
        ],
      },
    ],
  };
});

beforeEach(() => {
  dataStore.errorsMuted = false;
  jest.clearAllMocks();
  loadWorkbookSpy = jest
    .spyOn(utils, 'loadWorkbook')
    .mockResolvedValue(workbook);
  updateMock = jest.fn();
  whisperSpy = jest.spyOn(whisper, 'create').mockResolvedValue({
    id: 'dwa',
    update: updateMock,
    close: () => {},
    componentState: new Map(),
  });
});

describe('Parse system', () => {
  it('Does display whispers if errors', async () => {
    const system = createParsingSystem(
      'path',
      [
        {
          name: 'test',
          parser: () => {
            throw {message: 'Message Test'};
          },
        },
      ],
      dataStore,
      errorMessageLabel
    );
    await system.loadAndParse();
    expect(whisperSpy).toBeCalled();
    expect(updateMock).toBeCalled();
    expect(JSON.stringify(updateMock.mock.calls[0][0])).toContain(
      'Message Test'
    );
  });
  it('File missing causes whisper', async () => {
    loadWorkbookSpy.mockRejectedValue({message: "Can't load"});
    const system = createParsingSystem(
      'path',
      [],
      dataStore,
      errorMessageLabel
    );
    await system.loadAndParse();
    expect(whisperSpy).toBeCalled();
    expect(updateMock).toBeCalled();
    expect(JSON.stringify(updateMock.mock.calls[0][0])).toContain("Can't load");
  });
  it('Errors can be muted', async () => {
    const system = createParsingSystem(
      'path',
      [
        {
          name: 'test',
          parser: () => {
            throw {message: 'Message Test'};
          },
        },
      ],
      dataStore,
      errorMessageLabel
    );
    await system.loadAndParse();
    expect(whisperSpy).toBeCalled();
    expect(updateMock).toBeCalled();

    const components: Component[] = updateMock.mock.calls[0][0].components;
    const box = components[components.length - 1];

    assertComponentIsBox(box);
    const button = box.children[0];
    assertComponentIsButton(button);

    button.onClick(undefined, {
      id: 'dwa',
      update: () => {},
      close: () => {},
      componentState: new Map(),
    });

    jest.clearAllMocks();
    await system.loadAndParse();
    expect(whisperSpy).not.toBeCalled();
    expect(updateMock).not.toBeCalled();
  });
  it('Does not display whispers if no errors', async () => {
    const system = createParsingSystem(
      'path',
      [],
      dataStore,
      errorMessageLabel
    );
    await system.loadAndParse();
    expect(whisperSpy).not.toBeCalled();
  });
});
