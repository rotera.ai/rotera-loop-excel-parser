/* eslint-disable node/no-unpublished-import */
/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Workbook} from '@oliveai/ldk/dist/document/types';
import {ParseResult} from '../src/types/parseResult';
import {ResourceResult} from '../src/types/resourceResult';
import {ParserConfig} from '../src/types/parseConfig';
import {BaseWorkbookParser} from '../src/index';

export class VisitTypesTestParser extends BaseWorkbookParser<VisitTypesTestDataResult> {
  constructor(workbook: Workbook, workbookName: string, config: ParserConfig) {
    super(workbook, workbookName, config);
  }

  public mapper(data: ParseResult): ResourceResult<VisitTypesTestDataResult> {
    const tempData: VisitTypesTestDataResult = {
      visitTypes: data.data.map(x => {
        return {
          category: x.category,
          visitType: x.visitType,
          reasonForVisit: x.reasonForVisit,
          notes: x.notes,
          scheduleAtNav: x.scheduledAtNavCenter,
        };
      }),
    };

    return {
      header: data.header,
      data: tempData,
    };
  }
}

export type VisitTypesTestData = {
  category: string;
  visitType: string;
  reasonForVisit: string;
  scheduleAtNav: string;
  notes?: string;
};

export type VisitTypesTestDataResult = {
  visitTypes: VisitTypesTestData[];
};

export class MedicareCpeTestParser extends BaseWorkbookParser<MedicareCpeSheetTestDataResult> {
  constructor(workbook: Workbook, workbookName: string, config: ParserConfig) {
    super(workbook, workbookName, config);
  }

  public mapper(
    data: ParseResult
  ): ResourceResult<MedicareCpeSheetTestDataResult> {
    const tempData: MedicareCpeSheetTestDataResult = {
      listMessage: data.originalData[0][0],
      insuranceQualification: data.data.map(x => {
        return {
          insurance: x.insurance,
          qualifyForCpe: x.qualifyForCpe,
        };
      }),
    };

    return {
      header: data.header,
      data: tempData,
    };
  }
}

type MedicareCpeSheetTestData = {
  insurance: string;
  qualifyForCpe: string;
};

export type MedicareCpeSheetTestDataResult = {
  listMessage: string;
  insuranceQualification: MedicareCpeSheetTestData[];
};
