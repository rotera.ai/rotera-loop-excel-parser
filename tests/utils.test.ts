/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {findMissingHeaders, assertHeaders} from '~/utils';
describe('Assert headers', () => {
  test('Does not throw if all headers found', () => {
    const headers = ['header1', 'header2'];
    const requiredHeaders = ['header1', 'header2'];
    expect(() => assertHeaders(headers, requiredHeaders)).not.toThrow();
  });

  test('Throws if missing headers', () => {
    const headers = ['header1', 'header2'];
    const requiredHeaders = ['header1', 'header2', 'header3'];
    expect(() => assertHeaders(headers, requiredHeaders)).toThrow(
      'Missing headers: header3. Required headers: header1, header2, header3'
    );
  });
});

describe('Find missing headers', () => {
  test('Does not return anything if all headers found', () => {
    const headers = ['header1', 'header2'];
    const requiredHeaders = ['header1', 'header2'];
    expect(findMissingHeaders(headers, requiredHeaders)).toEqual([]);
  });

  test('Returns missing headers', () => {
    const headers = ['header1', 'header2'];
    const requiredHeaders = ['header1', 'header2', 'header3'];
    expect(findMissingHeaders(headers, requiredHeaders)).toEqual(['header3']);
  });
});
