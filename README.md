# Rotera Excel Parser

This package allows you to easily parse a given Excel file.

You are required to extend the Base Parser and implement the mapper function.

Extending Base Parser
---
```
export class YourParser extends BaseParser<YourDataClass> {

    constructor(
        workbook: Workbook,
        workbookName: ExcelTabName,
        config: parserConfig) {
        super(workbook, workbookName, config);
    }
```


The BaseParser constructor expects 3 parameters

    1.  An Exceljs `Workbook`
    2.  The name of the Excel Tab to parse
    3.  ParserConfig object

- The Parser Config object expects the following properties:

    1. header - The header has 2 properties and instructs the parser how to parse the headers
        1.1  userHeader: `<true>` | `<false>`
        1.2  headerRow: The row number of the header in the Excel file

    2. dataStartRow - the starting row number where the data row begins 
    3. customHeader - Allows you to override the naming convention of the Header
        3.1  userHeader: `<true>` | `<false>`
        3.2  headerRow: The row number of the header in the Excel file
    4. customDefinedHeader - Custom Defined Headers allow you to provide a custom header
        - Custom Defined header expects a string[]
        ex ["Header1 Name", "Header2 Name"]`

TODO: Add documentation about mapper function.

Parsing
----
- To Parse the excel file you call the parse()

# Publishing a new npm package

NPM packages are piblished automatically if commit messages are given in the format below.

# Commit Messages

We use **[semantic-release](https://github.com/semantic-release/semantic-release)** to generate version numbers and releases by parsing our commit messages in the [Angular JS commit message format](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).

Here is an example of the release type that will be done based on a commit messages:

| Commit message                                                                                                                                                                                   | Release type               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |

See the [semantic-release](https://github.com/semantic-release/semantic-release) docs for more information.
