/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {headerParseConfig} from './headerParseConfig';

export type ParserConfig = {
  header?: headerParseConfig;
  dataStartRow: number;
  customHeader?: headerParseConfig;
  customDefinedHeader?: string[];
};
