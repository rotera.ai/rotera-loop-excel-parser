/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Workbook} from '@oliveai/ldk/dist/document/types';

export type ParseDataProvider = {
  name: string;
  parser: (workbook: Workbook) => unknown;
};

export type DataStore = {
  errorsMuted: boolean;
  launchWhisperSent: boolean;
};
