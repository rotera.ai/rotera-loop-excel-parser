/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

/**
 * The result of an Excel parse.
 */
export type ParseResult = {
  /**
   * The headers in the sheet.
   */
  header: string[];

  /**
   * Any custom headers the sheet may have.
   */
  customHeader?: string[];

  /**
   * The map from column header names to the column number that contains that header.
   */
  headerMap: Map<string, number>;

  /**
   * The row data mapped to objects.
   */
  data: Record<string, string>[];

  /**
   * The raw data that is mapped into objects.
   */
  rawData: string[][];

  /**
   * The raw data from the sheet, including rows not mapped into objects.
   */
  originalData: string[][];
};
