/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {Component, WhisperComponentType} from '@oliveai/ldk/dist/whisper';

function assertComponentType(
  whisper: Component,
  expectedType: WhisperComponentType
): void {
  if (whisper.type !== expectedType) {
    throw `Expected whisper type '${expectedType}', got '${whisper.type}'`;
  }
}

export function assertComponentIsBox(
  component: Component
): asserts component is whisper.Box {
  return assertComponentType(component, WhisperComponentType.Box);
}
export function assertComponentIsButton(
  component: Component
): asserts component is whisper.Button {
  return assertComponentType(component, WhisperComponentType.Button);
}
