/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Workbook} from '@oliveai/ldk/dist/document/types';
import {filesystem, network, document} from '@oliveai/ldk';
import {WriteOperation} from '@oliveai/ldk/dist/filesystem';
import {DataStore} from '../src/types/parsingSystemTypes';
/**
 * Locate missing headers in a given list
 * @param headers
 * @param requiredHeaders
 * @returns missing headers in a list
 */
export function findMissingHeaders(
  headers: string[],
  requiredHeaders: string[]
) {
  const headerSet = new Set(headers.map(item => item.trim()));
  const missing: string[] = [];
  requiredHeaders.forEach(requiredHeader => {
    if (!headerSet.has(requiredHeader)) {
      missing.push(requiredHeader);
    }
  });

  return missing;
}

/**
 * Find missing headers in the given list. Throws if missing.
 *
 * @param headers list of headers in file
 * @param requiredHeaders required headers
 * @throws message with missing and required headers
 */
export function assertHeaders(headers: string[], requiredHeaders: string[]) {
  const missing = findMissingHeaders(headers, requiredHeaders);
  if (missing.length > 0) {
    throw new Error(
      `Missing headers: ${missing.join(
        ', '
      )}. Required headers: ${requiredHeaders.join(', ')}`
    );
  }
}
/**
 * Load workbook
 *
 * @param path location of workbook
 */
export async function loadWorkbook(path: string): Promise<Workbook> {
  const byteArray = await filesystem.readFile(path);
  return await document.xlsxDecode(byteArray);
}

/**
 * @param dataStore
 * @param backupFilePath
 * Create backup from contents of dataStore
 * Because dataStore is just a plain object we can easily serialize it to JSON
 */
export async function backupDataFile(
  dataStore: DataStore,
  backupFilePath: string
) {
  try {
    const backupCopyDataStore: DataStore = {
      ...dataStore,
      errorsMuted: false,
      launchWhisperSent: false,
    };
    await filesystem.writeFile({
      path: backupFilePath,
      data: JSON.stringify(backupCopyDataStore),
      writeOperation: WriteOperation.overwrite,
      writeMode: 0o755,
    });
  } catch (e) {
    console.error('Could not create backup: ', e);
  }
}

/**
 * @param dataStore
 * @param backupFilePath
 * Restore from JSON backup file
 */
export async function restoreDataFile(
  dataStore: DataStore,
  backupFilePath: string
) {
  const bytes = await filesystem.readFile(backupFilePath);
  const string = await network.decode(bytes);
  const json = JSON.parse(string);
  Object.assign(dataStore, json);
}
