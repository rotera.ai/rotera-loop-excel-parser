/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {whisper} from '@oliveai/ldk';
import {
  Component,
  Direction,
  JustifyContent,
  Urgency,
  WhisperComponentType,
} from '@oliveai/ldk/dist/whisper';
import {loadWorkbook} from '../utils';
import {DataStore, ParseDataProvider} from '../types/parsingSystemTypes';

function createErrorPairComponent(
  label: string,
  error: string | {message?: string}
): Component {
  const text = typeof error === 'string' ? error : error.message;
  return {
    type: WhisperComponentType.ListPair,
    label: label,
    value: text ?? 'Unknown error',
    copyable: false,
    style: Urgency.Warning,
  };
}

/**
 * Creates whisper displaying file errors
 *
 */
function createPausedErrorsWhisper() {
  whisper
    .create({
      label: 'Errors Paused',
      components: [
        {
          type: WhisperComponentType.Markdown,
          body: 'File errors will not be displayed until Loop restart.',
        },
      ],
    })
    .then();
}

/**
 * Returns whisper displaying the number of errors occurred while loading new data
 *
 */
function getErrorCountComponent(issues: Component[]): Component {
  return {
    type: WhisperComponentType.Message,
    body: `${issues.length} error${
      issues.length > 1 ? 's' : ''
    } occurred while loading new data:`,
  };
}

/**
 * @param path excel file location
 * @param dataProviders tabs in spreadsheet
 * @param dataStore base properties of data
 * @param errorMessageLabel Label for error whisper
 * Create parsing system that will parse an excel spread sheet
 */
export function createParsingSystem(
  path: string,
  dataProviders: ParseDataProvider[],
  dataStore: DataStore,
  errorMessageLabel: string
) {
  let lastWhisper: whisper.Whisper | undefined;

  /**
   * Returns whisper to pause errors to be displayed
   *
   */
  function getMuteButtonComponent(): Component {
    return {
      type: WhisperComponentType.Box,
      children: [
        {
          type: WhisperComponentType.Button,
          label: 'Pause Errors',
          onClick: () => {
            dataStore.errorsMuted = true;
            lastWhisper?.close(() => {});
            createPausedErrorsWhisper();
          },
        },
      ],
      justifyContent: JustifyContent.Center,
      direction: Direction.Horizontal,
    };
  }

  /**
   * @param path excel file location
   * @param errorMessageLabel Label for error whisper
   * Reparses spreadsheet
   *
   */
  async function reload(
    path: string,
    errorMessageLabel: string
  ): Promise<boolean> {
    const issues: Component[] = [];
    try {
      console.log(`Beginning parse: ${path}`);
      const workbook = await loadWorkbook(path);

      dataProviders.forEach(provider => {
        const providerName = provider.name;
        try {
          provider.parser(workbook);
        } catch (e: any) {
          console.error(
            `Parse Provider Error: ${providerName} - ${JSON.stringify(e)}`
          );
          issues.push(createErrorPairComponent(providerName, e));
        }
      });
    } catch (e: any) {
      issues.push(createErrorPairComponent('File', e));
    }

    if (issues.length) {
      if (dataStore.errorsMuted) return false;
      if (!lastWhisper) {
        lastWhisper = await whisper.create({
          label: errorMessageLabel,
          components: [],
          onClose: () => {
            lastWhisper = undefined;
          },
        });
      }

      lastWhisper.update({
        components: [
          getErrorCountComponent(issues),
          ...issues,
          {type: WhisperComponentType.Divider},
          getMuteButtonComponent(),
        ],
      });
      return false;
    } else {
      console.log(`Parse Success: ${path}`);
      lastWhisper?.close(() => {});
    }
    return true;
  }

  return {
    loadAndParse: () => reload(path, errorMessageLabel),
  };
}
