/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Workbook, Worksheet} from '@oliveai/ldk/dist/document/types';
import {camelCase} from 'lodash';
import {Constants} from '../constants';
import {ParserConfig} from '../types/parseConfig';
import {ParseHeaderResult} from '../types/parseHeaderResult';
import {ParseResult} from '../types/parseResult';
import {ResourceResult} from '../types/resourceResult';

/**
 * @class Base Parser is a Abstract Generic class
 * for building concrete Parsers for Excel spreadsheets.
 *
 * @augments <T> is return type of Data
 * @author  Davis Sylvester
 * @version 1.0
 * @since   2021-06-06
 */
export abstract class BaseWorkbookParser<T> {
  protected worksheet: Worksheet;
  protected totalRow: number;

  constructor(
    private workbook: Workbook,
    protected worksheetName: string,
    protected config: ParserConfig
  ) {
    const foundWorksheet = this.findWorksheetByName(worksheetName);
    if (!foundWorksheet) {
      throw new Error(Constants.WORKSHEET_NOT_FOUND);
    }

    this.worksheet = foundWorksheet;
    this.totalRow = this.getMaxRows();
  }

  private findWorksheetByName(worksheetName: string): Worksheet | undefined {
    const result = this.workbook.worksheets.find(
      x => x.name.toLowerCase() === worksheetName.toLowerCase()
    );

    return result;
  }

  private getMaxRows(): number {
    return this.worksheet.rows.length;
  }

  /**
   * @function parse
   *
   *  Parse the excel file for a given tab
   *  @returns {ResourceResult<T>}
   */
  public parse(): ResourceResult<T> {
    const innerData = this.innerParse();

    const result = this.mapper(innerData);

    return result;
  }

  private innerParse(): ParseResult {
    const headerResult = this.parseHeader(this.config);

    const originalData: string[][] = [];

    const rawData: string[][] = [];

    const result: Record<string, string>[] = [];

    const actualDataStartRow = this.config.dataStartRow - 1 || 1;

    // Read all rows that won't be mapped.
    for (let i = 0; i < actualDataStartRow; i++) {
      // puts each row int a ['Cell1', 'cell2']
      const tempCellsAsStrings: string[] = [];
      this.worksheet.rows[i].cells.map(value => {
        tempCellsAsStrings.push(value.value);
      });
      originalData.push(tempCellsAsStrings);
    }

    // Map all data from the sheet into their appropriate domain objects..
    for (let i = actualDataStartRow; i < this.totalRow; i++) {
      const workingObj: Record<string, string> = {};

      const tempCellsAsStrings: string[] = [];
      this.worksheet.rows[i].cells.map(value => {
        tempCellsAsStrings.push(value.value);
      });
      rawData.push(tempCellsAsStrings);
      originalData.push(tempCellsAsStrings);

      headerResult.header?.forEach((header, index) => {
        workingObj[camelCase(header)] = tempCellsAsStrings[index];
      });

      result.push(workingObj);
    }

    const header = headerResult.header ?? [];
    const customHeader = headerResult.customHeader;
    const headerMap = headerResult.headerMap;

    return {
      header: header,
      customHeader: customHeader,
      headerMap: headerMap,
      data: result,
      rawData: rawData,
      originalData: originalData,
    };
  }

  private parseHeader(config: ParserConfig): ParseHeaderResult {
    let header: string[] | undefined = undefined;
    let customHeader: string[] | undefined = undefined;
    let createdHeaderDictionary: Map<string, number> = new Map<
      string,
      number
    >();

    if (config.header && config.header.useHeader) {
      const tempHeader: string[] = [];
      this.worksheet.rows[config.header.headerRow - 1].cells.map(value => {
        tempHeader.push(value.value);
      });
      const headerDictionary = this.createHeaderDictionary(tempHeader);
      if (headerDictionary) createdHeaderDictionary = headerDictionary;

      header = this.removeEmptyHeaderElement(tempHeader);
    }

    if (config.customHeader && config.customHeader.useHeader) {
      const tempCustomHeader: string[] = [];
      this.worksheet.rows[config.customHeader.headerRow - 1].cells.map(
        value => {
          tempCustomHeader.push(value.value);
        }
      );

      customHeader = this.removeEmptyHeaderElement(tempCustomHeader);
    } else {
      customHeader = header;
    }

    if (config.customDefinedHeader) {
      header = config.customDefinedHeader;
      customHeader = config.customDefinedHeader;
    }

    return {
      header: header,
      customHeader: customHeader,
      headerMap: createdHeaderDictionary,
    };
  }

  private createHeaderDictionary(tempHeader: string[]) {
    let headerRow: Map<string, number> | null = null;

    if (tempHeader && tempHeader.length > 0) {
      headerRow = new Map<string, number>();
      tempHeader.forEach((x: string, idx: number) => {
        if (headerRow) headerRow.set(camelCase(x), idx);
      });
    }

    return headerRow;
  }

  protected abstract mapper(data: ParseResult): ResourceResult<T>;

  private removeEmptyHeaderElement(data: string[]): string[] {
    if (!data || data.length <= 1) {
      return data;
    }
    // TODO: figure out the purpose of this line.
    // const result = data.filter((_, idx) => idx !== 0);

    return data;
  }
}
